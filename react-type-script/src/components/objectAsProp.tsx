//passing an Object as prop
type personProps={
    name:{
        first:string
        lastt:string
    }
}
export const Person=(props:personProps)=>{
    return(
        <div>
           <h1>{props.name.first}{props.name.lastt}</h1>
        </div>
    )
}