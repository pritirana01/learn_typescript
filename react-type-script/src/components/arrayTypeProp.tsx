import React from 'react';

type StudentProps = {
  names: {
    name: string;
    grade: string;
  }[];
};

export const Student= (props:StudentProps) => {
  return (
    <div>
      {props.names.map((student) => (
        <h1 key={student.name}>{student.name} {student.grade}</h1>
      ))}
    </div>
  );
};
