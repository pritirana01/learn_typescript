//conditionally render data based on status 
type StatusProps={
    status:'loading'| 'success' | 'error'
}
export const StatusData=(props:StatusProps)=>{
    let message
    if(props.status==='loading')
    {
        message='loading  data'
    }
    else if(props.status==='success'){
        message='data fetched successfully '

    }
    else if(props.status==='error'){
        message='some error is occured'
    }
    return(
        <div>
            <h1>status is -{message}</h1>
          
          
        </div>
    )
}